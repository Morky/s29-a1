const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

let users = [];

app.get('/home',(req, res)=>{
	res.send(`This is home.`);
})

app.get('/users',(req, res)=>{
	res.send(users);
})

app.post('/users',(req, res)=>{
	if (req.body.lastName !== '' && req.body.firstName !== '' && req.body.email !== '') {
		users.push(req.body);
		res.send(`Welcome ${req.body.firstName}! Your account is ready!`);
	}else{
		res.send(`Make sure to provide them all.`)
	}
})

app.listen(port,()=>console.log(`Server running at ${port}`));